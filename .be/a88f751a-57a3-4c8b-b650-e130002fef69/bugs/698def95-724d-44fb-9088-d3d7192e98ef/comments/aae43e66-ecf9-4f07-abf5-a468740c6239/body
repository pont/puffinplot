On-screen superscripts are fine in recent OS X Java versions, but
printing on OS X Java 7 is hopeless: it appears that every superscript
character is shifted down by about five line-heights, with the offset
compounding for each successive character! The most obvious workaround
is of course to export a PDF file and print that, and I can't think of a
use case which wouldn't be served by this technique. But it's still
unpleasant to have an obvious bug.

First thing to do is to test on OS X Java 8. If it works there, there's
no problem, since Java 8 is currently the default version for OS X.
(Java 7 will be retired in April 2015.)

Failing that, I should add a workaround to the plotting code
specifically for printing on OS X. It would be possible to reinstate the
"fake" superscript I used to use for OS X Java 5 (which had a similar
bug) -- however, this was always a little fragile, since the affine
transform implementations that it used were also buggy. Since I don't
have the resources to test much on OS X (and since probably few people
care about this bug) the best compromise is probably to use "e" notation
on Mac printouts -- e.g. "e-5" for "×10⁻⁵". It's tempting simply to use
the Unicode code points for pre-superscripted digits and minus sign, but
unfortunately not all fonts have glyphs for these.

