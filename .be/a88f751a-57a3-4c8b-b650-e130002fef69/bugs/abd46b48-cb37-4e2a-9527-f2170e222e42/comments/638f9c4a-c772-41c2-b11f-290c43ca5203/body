Over the years, the Preferences architecture has accreted into
a bit of a mess. This is a meta-bug for various things which could
be solved by a rewrite. Sub-bugs I have found relating to
Preferences:

54: Save window position on exit
57: Tidy up preferences architecture
130: Move most preferences to Suite?
201: General API for plot options

There is also: the necessity of duplicating default settings in the
preferences window and the actual class that uses the preference,
with mismatches leading to bugs like #233; and the fact that many
preferences use the same string to represent the programmatically
parsed setting and the representation displayed to the user. The
latter is due to a lazy implementation of
PrefsWindow.makeLabelledPrefComboBox and would cause trouble for
internationalization among other things.
