Double.toString() and Double.parseDouble(...) appear to be locale-unaware
and therefore safe for data reading and writing (no fear of commas as
decimal separators, etc.). The dangerous bits in PuffinPlot are 
String.format and Scanner, which default to the current locale if none
is specified explicitly. Need to go through these making sure a standard
locale (presumably "en") is set everywhere so that PP file formats are
not dependent on current locale. toString and parseDouble should be 
preferred where possible for reading and writing files: apart from avoiding
locale hassles, they provide maximum accuracy without extraneous digits
and with no headaches about "appropriate" SF/DP values in format specifiers.
