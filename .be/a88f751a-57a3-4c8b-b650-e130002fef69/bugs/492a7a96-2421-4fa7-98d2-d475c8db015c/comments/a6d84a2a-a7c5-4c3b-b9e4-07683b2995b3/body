I've performed some tests with jfreesvg, vectorgraphics2d, and
orsonpdf, as well as the three existing libraries. The biggest 
problem appears to be superscripts in attributed text objects.
Only FreeHEP handles them correctly; other libraries either ignore
them or render every attributed text object as a path.

The evaluation suggests to me that I should keep FreeHEP and drop
the other two existing libraries (which seem to have inferior
performance). I should also add the EPS and EMF backends for 
FreeHEP export, since these formats might be more tractable
than PDF and SVG for many users, and with the main FreeHEP library
already included the extra backends are relatively lightweight.

The most compelling feature of JFreeSVG is the ability to explicitly
control grouping via hints. This would be very convenient for 
grouping the elements of each plot, and none of the other libraries
can do it. Batik puts everything in one group, which isn't too
inconvenient. Batik creates bizarre subgroups of seemingly 
unrelated elements, which necessitates full ungrouping before 
any useful editing can be done. Despite this, I still think the
best choice is to standardize on FreeHEP for now: the correct
attributed string handling and multiple backend support are heavily
in its favour.
