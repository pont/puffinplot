There is now a rough model for this API, in the shape of the options API
recently implemented for the file importers. The file importer option API
may even be reusable with minor modification for plot options -- I could
split the FileLoader interface into a file-reading interface and a
reusable option-handling interface.

It should work something like this:

1. Options are set per plot instance, not per class. (Currently this makes
   no difference, but eventually it will be possible to make multiple
   instances of each plot type.)

2. The user gets to the plot options via the instance (e.g. by
   right-clicking on the plot display), not via the preferences window.

3. Plots declare their option names, types, and default values via
   an implemented interface (as already done for file importers).

4. The Puffin app autogenerates an option dialog from the option
   description map supplied by the instance. (Also applicable to file
   importers.)

5. Option interface should include a getCurrentOptionValues method to let
   the Puffin app serialize current state on exit. The Java preferences
   API supports byte stream values so this shouldn't be hard. Options
   can be set on startup either via constructor (disadvantage: can't be
   enforced in interface) or setter (disadvantage: initialization is a
   two-step process of construction and option setting).

